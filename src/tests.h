//
// Created by ilya on 1/9/23.
//

#ifndef MEMORY_ALLOCATOR_SRC_TESTS_H_
#define MEMORY_ALLOCATOR_SRC_TESTS_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "mem.h"
#include "mem_internals.h"

bool run_all_tests(void);

#endif //MEMORY_ALLOCATOR_SRC_TESTS_H_
