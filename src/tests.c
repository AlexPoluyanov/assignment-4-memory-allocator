#define _DEFAULT_SOURCE

#include "tests.h"

#define HEAP_SIZE 1000
#define ALLOC_SIZE_DEFAULT 100

static struct block_header *get_block(void *block) {
  return (struct block_header *) (block - offsetof(struct block_header, contents));
}

void *heap = NULL;

bool run_test(FILE *out, bool (*test_func)(void)) {
  heap = heap_init(HEAP_SIZE);
  if (heap == NULL) {
    return false;
  }
  debug_heap(out, heap);
  bool result = test_func();
  debug_heap(out, heap);
  fprintf(out, "Test %s\n----------------------\n", result ? "passed" : "failed");
  return result;
}

bool test_1(void) {
  void *mem = _malloc(ALLOC_SIZE_DEFAULT);
  struct block_header *block = get_block(mem);
  if (mem == NULL || block->capacity.bytes != ALLOC_SIZE_DEFAULT) {
    return false;
  }
  _free(mem);
  return true;
}

bool test_2(void) {
  void *mem1 = _malloc(ALLOC_SIZE_DEFAULT);
  struct block_header *block = get_block(mem1);
  if (mem1 == NULL || block->capacity.bytes != ALLOC_SIZE_DEFAULT) {
    return false;
  }
  _free(mem1);
  if (block->is_free != true) {
    return false;
  }
  void *mem2 = _malloc(ALLOC_SIZE_DEFAULT);
  struct block_header *block2 = get_block(mem2);
  if (mem2 == NULL || block2->capacity.bytes != ALLOC_SIZE_DEFAULT) {
    return false;
  }
  _free(mem2);
  return true;
}

bool test_3(void) {
  void *mem1 = _malloc(ALLOC_SIZE_DEFAULT);
  struct block_header *block = get_block(mem1);
  if (mem1 == NULL || block->capacity.bytes != ALLOC_SIZE_DEFAULT) {
    return false;
  }
  void *mem2 = _malloc(ALLOC_SIZE_DEFAULT);
  struct block_header *block2 = get_block(mem2);
  if (mem2 == NULL || block2->capacity.bytes != ALLOC_SIZE_DEFAULT) {
    return false;
  }
  _free(mem2);
  if (block2->is_free != true) {
    return false;
  }
  _free(mem1);
  if (block->is_free != true) {
    return false;
  }
  void *mem3 = _malloc(ALLOC_SIZE_DEFAULT);
  struct block_header *block3 = get_block(mem3);
  if (mem3 == NULL || block3->capacity.bytes != ALLOC_SIZE_DEFAULT) {
    return false;
  }
  _free(mem3);
  return true;
}

bool test_4(void) {
  void *mem1 = _malloc(HEAP_SIZE + 1);
  struct block_header *block1 = get_block(mem1);

  if (mem1 == NULL || block1->capacity.bytes != HEAP_SIZE + 1)
    return false;

  void *mem2 = _malloc(ALLOC_SIZE_DEFAULT);
  struct block_header *block2 = get_block(mem2);

  if (mem2 == NULL || block2->capacity.bytes != ALLOC_SIZE_DEFAULT)
    return false;

  _free(mem1);
  _free(mem2);
  return true;
}

bool test_5(void) {
  void *mem1 = _malloc(HEAP_SIZE);
  struct block_header *block = get_block(mem1);

  if (mem1 == NULL || block->capacity.bytes != HEAP_SIZE)
    return false;

  void *address = mmap((void *) heap + HEAP_SIZE, HEAP_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE
      | MAP_ANONYMOUS | MAP_FIXED, -1, 0);

  if (!address)
    return false;

  void *mem2 = _malloc(ALLOC_SIZE_DEFAULT);
  struct block_header *block2 = get_block(mem2);

  if (mem2 == NULL || block2->capacity.bytes != ALLOC_SIZE_DEFAULT)
    return false;

  _free(mem1);
  _free(mem2);
  return true;
}

bool run_all_tests() {
  bool success = true;
  success &= run_test(stdout, test_1);
  success &= run_test(stdout, test_2);
  success &= run_test(stdout, test_3);
  success &= run_test(stdout, test_4);
  success &= run_test(stdout, test_5);
  return success;
}

