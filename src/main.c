#include <stdio.h>

#include "tests.h"

int main()
{
  printf("%s", run_all_tests() ? "Tests passed" : "Tests failed");
  return 0;
}
